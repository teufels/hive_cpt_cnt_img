<?php
namespace HIVE\HiveCptCntImg\ViewHelpers;

/***
 *
 * This file is part of the "hive_cpt_cnt_img" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Albert Giss <a.giss@teufels.com>, teufels GmbH
 *           Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Bastian Holzem <b.holzem@teufels.com>, teufels GmbH
 *           Hendrik Krueger <h.krueger@teufels.com>, teufels GmbH
 *           Marcel Weber <m.weber@teudels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels>, teufels GmbH
 *
 ***/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use HIVE\HiveUserfuncs\UserFunc\SettingsUserFunc;
use TYPO3\CMS\Frontend\Service\TypoLinkCodecService;


/**
 * Class CptCntBsImgViewHelper
 * @package HIVE\HiveViewhelpers\ViewHelpers
 */
class CptCntImgViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper
{

    /**
     * falImageRepository
     *
     * @var \HIVE\HiveCptCntImg\Domain\Repository\FalImageRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $falImageRepository = NULL;

    /**
     * @var \TYPO3\CMS\Extbase\Service\ImageService
     */
    protected $imageService = null;

    /**
     * @param \TYPO3\CMS\Extbase\Service\ImageService $imageService
     */
    public function injectImageService(\TYPO3\CMS\Extbase\Service\ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function initializeArguments()
    {
        $this->registerArgument('uid', 'integer', 'Core File Reference uid', true);
        $this->registerArgument('dataAttribute', 'string', 'Data Attribute', false, 'echo');

    }

    /**
     * @return string
     */
    public function render() {

        $aArguments = $this->arguments;

        $uid = $aArguments['uid'];
        $dataAttribute = $aArguments['dataAttribute'];
        
        $aSettings = $this->templateVariableContainer->get('settings');

        /*
         * It is possible to use this ViewHelper in a custom extension.
         *
         * In this case we don't get our Flexform settings or maybe
         * wrong Flexform settings from the Flexform used to generate
         * frontend output.
         *
         * Therefor check against "useInCarousel" setting
         * and if not present do:
         */
        if (!is_array($aSettings) or (is_array($aSettings) and !array_key_exists("useInCarousel", $aSettings))) {
            /*
             * Empty Settings array
             */
            $aSettings = [];
            $aCleanFullTyposcript = SettingsUserFunc::getCleanFullTyposcriptArrayForPlugin('tx_hivecptcntimg_hivecptcntimgshowfalimage');
            $aSettings = $aCleanFullTyposcript["settings"];
        }

        if (!empty($uid)) {
            
            $resourceFactory = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\ResourceFactory');
            /* @var \TYPO3\CMS\Core\Resource\FileReference $oCoreFileReference */
            $oCoreFileReference = $resourceFactory->getFileReferenceObject($uid);
            $aFigure = [];
            if ($oCoreFileReference != NULL && get_class($oCoreFileReference) == 'TYPO3\\CMS\\Core\\Resource\\FileReference') {

                $cropString = $oCoreFileReference instanceof FileReference ? $oCoreFileReference->getProperty('crop') : null;
                $cropVariantCollection = CropVariantCollection::create((string)$cropString);
                $cropVariant = 'default';

                /*
                 * cropArea
                 */
                $cropArea = $cropVariantCollection->getCropArea($cropVariant);

                $processingInstructions = [
                    'crop' => $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($oCoreFileReference),
                ];
                $processedImage = $this->imageService->applyProcessingInstructions($oCoreFileReference, $processingInstructions);

                /*
                 * focusArea
                 */
                $focusArea = $cropVariantCollection->getFocusArea($cropVariant);
                $aFocusArea = $focusArea->asArray();

                /*
                 * Focus
                 *
                 *    0   1   2   3   4   5   6   7   8   9   10
                 *  0 x---+---+---+---+---+---+---+---+---+---+
                 *    |   |   |   |   |   |   |   |   |   |###| x: 100% / y: 0%
                 *  1 +---+---+---+---+---+---+---+---+---+---+
                 *    |   |   |   |   |   |   |   |   |   |   |
                 *  2 +---+---+---+---+---+---+---+---+---+---+
                 *    |   |   |   |   |   |   |   |   |   |   |
                 *  3 +---+---+---+---+---+---+---+---+---+---+
                 *    |   |   |   |   |   |   |   |   |   |   |
                 *  4 +---+---+---+---+---+---+---+---+---+---+
                 *    |   |   |   |   |   |   |   |   |   |   |
                 *  5 +---+---+---+---+---+---+---+---+---+---+
                 *    |   |   |   |   |   |   |   |   |   |   |
                 *  6 +---+---+---+---+---+---+---+---+---+---+
                 *    |   |   |   |   |   |   |   |   |   |   |
                 *  7 +---+---+---+---+---+---+---+---+---+---+
                 *    |   |   |   |   |   |   |   |   |   |   |
                 *  8 +---+---+---+---+---+---+---+---+---+---+
                 *    |   |   |   |   |   |   |   |   |   |   |
                 *  9 +---+---+---+---+---+---+---+---+---+---+
                 *    |   |   |   |   |   |   |   |   |   |   |
                 * 10 +---+---+---+---+---+---+---+---+---+---+
                 *
                 */
                $fX = $aFocusArea['x'] + ($aFocusArea['width'] / 2);
                $fY = $aFocusArea['y'] + ($aFocusArea['height'] / 2);

                /*
                 * Dimensions
                 */
                $iWidth = $processedImage->getProperty('width');
                $iHeight = $processedImage->getProperty('height');

                /*
                 * Background
                 */
                $sFit = 'background-size';
                $sPosition = 'background-position';
                $sCase = 'backgroundImg';

                /*
                 * Style
                 */
                $sStyle = $sFit . ': cover;' . $sPosition . ': ' . $fX * 100 . '% ' . $fY * 100 . '%;';

                /*
                 * Mode
                 */
                $sDimensions = ($sCase == 'img' ? 'width: 100%;' . 'height: auto;' : '');

                $iPrecision = 1;
                $iAspectRatio = round($iWidth / $iHeight, $iPrecision);
                $sClassAspectRatio = "aR free";
                $sMode = 'landscape';
                switch ($iAspectRatio) {
                    case round(21/5, $iPrecision):
                        $sClassAspectRatio = "aR aR--21_5";
                        break;
                    case round(21/9, $iPrecision):
                        $sClassAspectRatio = "aR aR--21_9";
                        break;
                    case round(16/9, $iPrecision):
                        $sClassAspectRatio = "aR aR--16_9";
                        break;
                    case round(4/3, $iPrecision):
                        $sClassAspectRatio = "aR aR--4_3";
                        break;
                    case round(3/2, $iPrecision):
                        $sClassAspectRatio = "aR aR--3_2";
                        break;
                    case round(1/1, $iPrecision):
                        $sClassAspectRatio = "aR aR--1_1";
                        $sMode = 'square';
                        break;
                    case round(2/3, $iPrecision):
                        $sClassAspectRatio = "aR aR--2_3";
                        $sMode = 'portrait';
                        break;
                    case round(3/4, $iPrecision):
                        $sClassAspectRatio = "aR aR--3_4";
                        $sMode = 'portrait';
                        break;
                    default:
                        $sMode = '';
                }

                $sMode .= " " . $sClassAspectRatio;

                $sUrl = $this->imageService->getImageUri($processedImage);
                $aFigure['aCoreFileReference'] = $oCoreFileReference;
                $aFigure['sId'] = md5($sUrl) . rand(0, 99999);
                $aFigure['sUrl'] = $sUrl;
                $aFigure['sSrc'] = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
                $aFigure['iWidth'] = $iWidth;
                $aFigure['iHeight'] = $iHeight;
                $aFigure['sClass'] = 'focuhila';
                $aFigure['sMode'] = $sMode;
                $aFigure['sStyle'] = $sStyle . ' ' . $sDimensions;

                return $this->returnHtml($sMode, $sCase, $aFigure, $dataAttribute, $aSettings);

            }

            return '';
            
        }

        return '';

    }

    private function returnHtml ($sMode = 'square', $sCase = 'img', $aFigure = [], $sDataAttribute = 'echo', $aSettings = []) {

        $sHtml = '';

        if (count($aFigure) > 0) {
            $sUri = $this->sUri($aFigure);
            $sA = $sUri != '' ? '<a href="' . $sUri . '" class="position_absolute"></a>': '';

            $sCaption = $aFigure['aCoreFileReference']->getDescription() != '' ? $aFigure['aCoreFileReference']->getDescription() : ''; //$aFigure['aCoreFileReference']->getDescription() : '';
            if ($sCaption != '') {
                $sCaption = '
                    <div class="'. $aSettings['classes']['div'] .'">
                        <div class="'. $aSettings['classes']['div1'] .'">
                            <div class="'. $aSettings['classes']['div2'] .'">
                                <div class="'. $aSettings['classes']['div3'] .'">
                                    ' . $sCaption . '
                                </div>
                            </div>
                        </div>
                    </div>
                ';
            }

            $sHtml = '<div class="tx-hive-cpt-cnt-img">
            <div class="' . $sMode . '">';

            switch($sCase) {
                case 'backgroundImg':
                    $sHtml .= $this->backgroundImgHtml($aFigure, $sDataAttribute, $sA, $sCaption);
                    break;
                default:
                    $sHtml .=
                        '<figure class="' . $aFigure['sClass'] . '">';
                    $sHtml .= $this->imgHtml($aFigure, $sDataAttribute, $sA, $sCaption);
                    $sHtml .=
                        '</figure>';

            }

            $sHtml .= '</div>
            </div>';
        }

        return $sHtml;
    }

    private function imgHtml ($aFigure = [], $sDataAttribute = 'echo', $sA = '', $sCaption = '') {
        return
            '<img src="' . $aFigure['sSrc'] . '"
                 data-' . $sDataAttribute . '="' . $aFigure['sUrl'] . '"
                 width="' . $aFigure['iWidth'] . '"
                 height="' . $aFigure['iHeight'] . '"
                 alt="' . $aFigure['aCoreFileReference']->getAlternative() . '"
                 title="' . $aFigure['aCoreFileReference']->getTitle() . '"
                 class="' . ($sDataAttribute == 'echo' ?  'b-lazy ':'') .'opacity_0"
                 id="' . $aFigure['sId'] . '"
                 style="' . $aFigure['sStyle'] . '"
             />
             ' . $sA . '
             <figcaption class="figcaption">'
                . $sCaption .
            '</figcaption>';
    }

    private function backgroundImgHtml ($aFigure = [], $sDataAttribute = 'echo', $sA = '', $sCaption = '') {
        return
            '<div role="img"
                 data-' . $sDataAttribute . '="' . $aFigure['sUrl'] . '"
                 aria-label="' . $aFigure['aCoreFileReference']->getAlternative() . '"
                 class="' . $aFigure['sClass'] . ($sDataAttribute == 'echo' ?  ' b-lazy':'') .' opacity_0"
                 id="' . $aFigure['sId'] . '"
                 style="' . $aFigure['sStyle'] . '">
                 ' . $sA . '
                 <div class="figcaption">'
                    . $sCaption .
                '</div>
             </div>';
    }

    private function sUri ($aFigure, $useCacheHash = true, $forceAbsoluteUrl = true) {

        $sUri = '';
        if ($aFigure['aCoreFileReference']->getLink()) {
            $parameter = $aFigure['aCoreFileReference']->getLink();
            $contentObject = GeneralUtility::makeInstance(ContentObjectRenderer::class);
            $sUri = $contentObject->typoLink_URL(
                [
                    'parameter' => self::createTypolinkParameterFromArguments($parameter),
                    'useCacheHash' => $useCacheHash,
                    'forceAbsoluteUrl' => $forceAbsoluteUrl,
                ]
            );
            return $sUri;
        }
        return $sUri;
    }

    /**
     * Transforms ViewHelper arguments to typo3link.parameters.typoscript option as array.
     *
     * @param string $parameter Example: 19 _blank - "testtitle with whitespace" &X=y
     * @param string $additionalParameters
     *
     * @return string The final TypoLink string
     */
    protected static function createTypolinkParameterFromArguments($parameter, $additionalParameters = '')
    {
        $typoLinkCodec = GeneralUtility::makeInstance(TypoLinkCodecService::class);
        $typolinkConfiguration = $typoLinkCodec->decode($parameter);

        // Combine additionalParams
        if ($additionalParameters) {
            $typolinkConfiguration['additionalParams'] .= $additionalParameters;
        }

        return $typoLinkCodec->encode($typolinkConfiguration);
    }


}
