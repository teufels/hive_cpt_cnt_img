<?php
defined('TYPO3_MODE') or die();

$extKey = 'hive_cpt_cnt_img';
$title = 'hive_cpt_cnt_img';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', $title);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntimg_domain_model_falimage', 'EXT:hive_cpt_cnt_img/Resources/Private/Language/locallang_csh_tx_hivecptcntimg_domain_model_falimage.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntimg_domain_model_falimage');