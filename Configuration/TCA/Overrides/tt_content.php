<?php
defined('TYPO3_MODE') or die();

/***************
 * Plugin
 ***************/
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'HIVE.HiveCptCntImg',
    'Hivecptcntimgshowfalimage',
    'hive :: FAL Image :: showFalImage'
);

$extKey = strtolower('hive_cpt_cnt_img');

$pluginName = strtolower('Hivecptcntimgshowfalimage');
$pluginSignature = str_replace('_', '', $extKey) . '_' . str_replace('_', '', $pluginName);
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'. $extKey . '/Configuration/FlexForms/Config.xml');