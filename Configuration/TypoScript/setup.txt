
plugin.tx_hivecptcntimg_hivecptcntimgshowfalimage {
	view {
		templateRootPaths.0 = {$plugin.tx_hivecptcntimg_hivecptcntimgshowfalimage.view.templateRootPath}
		partialRootPaths.0 = {$plugin.tx_hivecptcntimg_hivecptcntimgshowfalimage.view.partialRootPath}
		layoutRootPaths.0 = {$plugin.tx_hivecptcntimg_hivecptcntimgshowfalimage.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_hivecptcntimg_hivecptcntimgshowfalimage.persistence.storagePid}
	}
}

plugin.tx_hivecptcntimg._CSS_DEFAULT_STYLE (
	textarea.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	input.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	.tx-hive-cpt-cnt-img table {
		border-collapse:separate;
		border-spacing:10px;
	}

	.tx-hive-cpt-cnt-img table th {
		font-weight:bold;
	}

	.tx-hive-cpt-cnt-img table td {
		vertical-align:top;
	}

	.typo3-messages .message-error {
		color:red;
	}

	.typo3-messages .message-ok {
		color:green;
	}

)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin.tx_hivecptcntimg_hivecptcntimgshowfalimage {
	settings {
		bDebug = {$plugin.tx_hivecptcntimg_hivecptcntimgshowfalimage.settings.bDebug}
		maxWidth = {$plugin.tx_hivecptcntimg_hivecptcntimgshowfalimage.settings.maxWidth}
		maxHeight = {$plugin.tx_hivecptcntimg_hivecptcntimgshowfalimage.settings.maxHeight}
		classes {
			div = {$plugin.tx_hivecptcntimg_hivecptcntimgshowfalimage.settings.classes.div}
			div1 = {$plugin.tx_hivecptcntimg_hivecptcntimgshowfalimage.settings.classes.div1}
			div2 = {$plugin.tx_hivecptcntimg_hivecptcntimgshowfalimage.settings.classes.div2}
			div3 = {$plugin.tx_hivecptcntimg_hivecptcntimgshowfalimage.settings.classes.div3}
		}
	}
}