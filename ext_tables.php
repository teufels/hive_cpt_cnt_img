<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntImg',
            'Hivecptcntimgshowfalimage',
            'hive :: FAL Image :: showFalImage'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_cpt_cnt_img', 'Configuration/TypoScript', 'hive_cpt_cnt_img');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntimg_domain_model_falimage', 'EXT:hive_cpt_cnt_img/Resources/Private/Language/locallang_csh_tx_hivecptcntimg_domain_model_falimage.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntimg_domain_model_falimage');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder